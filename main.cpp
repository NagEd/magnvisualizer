#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "mockdataprovider.h"

#ifdef INCLUDE_3D_RENDER
#include <Qt3DQuickExtras/qt3dquickwindow.h>
#endif

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);
#ifdef INCLUDE_3D_RENDER
    Qt3DExtras::Quick::Qt3DQuickWindow view;
    view.setSource(QUrl("qrc:/3dRender.qml"));
    view.show();
#else
    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    MockDataProvider* dataProvider = new MockDataProvider();
    engine.rootContext()->setContextProperty("dataProvider",dataProvider);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);
#endif
    return app.exec();
}
