import QtQuick 2.0

ListModel {

    ListElement
    {
        name:"Data Raw"
        key:"dataRaw"
        iconPath:"icon.png"
    }

    ListElement
    {
        name:"Graphics"
        key:"graphics"
        iconPath:"graphics.png"
    }
    ListElement
    {
        name:"Settings"
        key:"settings"
        iconPath:"settings.png"
    }
    ListElement
    {
        name:"Exit"
        key:"exit"
        iconPath:"exit.png"
    }
}
