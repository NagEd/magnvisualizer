#ifndef DATAPROVIDER_H
#define DATAPROVIDER_H

#include <QObject>
#include "abstractdataprovider.h"

#include "magnetometer.h"

class DataProvider : public QObject, public AbstractDataProvider
{
    Q_OBJECT
public:
    explicit DataProvider(QObject *parent = nullptr);
    void processData(QString data) override;
signals:
private:
    Magnetometer m_magnetometer;
};

#endif // DATAPROVIDER_H
