#ifndef ABSTRACTDATAPROVIDER_H
#define ABSTRACTDATAPROVIDER_H
#include <QString>

class AbstractDataProvider
{
    virtual void processData (QString data) = 0;
};

#endif // ABSTRACTDATAPROVIDER_H
