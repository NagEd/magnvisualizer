#ifndef MOCKDATAPROVIDER_H
#define MOCKDATAPROVIDER_H

#include <QObject>
#include "abstractdataprovider.h"
#include "magnetometer.h"

class QTimer;
class MockDataProvider : public QObject , public AbstractDataProvider
{
    Q_OBJECT
    Q_PROPERTY(QString displayValues READ displayValues WRITE setDisplayValues NOTIFY displayValuesChanged)
public:
    explicit MockDataProvider(QObject *parent = nullptr);
    void processData ( QString data ) override;

    inline QString displayValues() const { return m_dataText; }

    void setDisplayValues(QString values);

    void sampleDataHandler(double sample);
signals:
        void displayValuesChanged();
private:
    QTimer * m_UpdateTimer;
    QList<double> m_data;
    QString m_dataText;
    Magnetometer m_magnetometer;
};

#endif // MOCKDATAPROVIDER_H
