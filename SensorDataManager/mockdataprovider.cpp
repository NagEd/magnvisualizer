#include "mockdataprovider.h"
#include <QTimer>
#include <QDebug>
#include <QVariantMap>

MockDataProvider::MockDataProvider(QObject *parent) : QObject(parent)
{
    m_UpdateTimer = new QTimer();
    m_UpdateTimer->setInterval(1000);
    m_UpdateTimer->setSingleShot(false);
    connect(m_UpdateTimer,&QTimer::timeout,this,[=](){
        processData(QString::number((rand() %1000) / 1.7) );
    });
    m_UpdateTimer->start();

    SensorDataHandler handler = [this] (double sample)-> void { MockDataProvider::sampleDataHandler(sample);};
    QVariantMap configMap;
    configMap["bufferSize"] = 128;
    configMap["fullScale"] = 2000.0;
    configMap["availableODRs"] = QStringList{"100","200","400"};

    m_magnetometer.initSensor(configMap);
    m_magnetometer.registerDataHandler(handler);
}

void MockDataProvider::processData(QString data)
{
    qDebug() << Q_FUNC_INFO << data.toDouble();
    m_magnetometer.handleNewSampleReceived(data.toDouble());
    emit displayValuesChanged();
}

void MockDataProvider::setDisplayValues(QString values){
    m_dataText = values;
    emit displayValuesChanged();
}


void MockDataProvider::sampleDataHandler(double sample)
{
    qDebug()<< Q_FUNC_INFO <<"handler called"<< sample;
    m_dataText += QString::number(sample) + " ";
}
