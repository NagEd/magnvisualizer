#include "magnetometer.h"
#include <QVariantMap>
Magnetometer::Magnetometer(QObject *parent):QObject(parent)
{

}

void Magnetometer::initSensor(QVariantMap configuration)
{
    if(configuration.contains("bufferSize"))
    {
        setBufferSize(configuration["bufferSize"].toInt());
    }
}

QList<double> Magnetometer::samples()
{
    return m_samples;
}

void Magnetometer::initSamples()
{
  Q_ASSERT(m_bufferSize>0);
    if(m_samples.count() >0)
    {
        m_samples.clear();
    }

  for(int index = 0 ; index < m_bufferSize ; index++)
  {
      m_samples.append(0);
  }
}


void Magnetometer::registerDataHandler(SensorDataHandler handler)
{
    m_handlers.append(handler);
}

void Magnetometer::deleteHandlers()
{
    m_handlers.clear();
}

void Magnetometer::handleNewSampleReceived ( double sample )
{
    for(SensorDataHandler handler : m_handlers)
    {
//        qDebug()<< Q_FUNC_INFO<<sample;
        (handler)(sample);
        if(m_samples.count() == m_bufferSize)
        {
            m_samples.removeFirst();
        }
        m_samples.append(sample);
    }
}
