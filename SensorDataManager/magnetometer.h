#ifndef MAGNETOMETER_H
#define MAGNETOMETER_H

#include <QObject>

#include <functional>

typedef std::function <void (double sensorData)> SensorDataHandler ;

class Magnetometer:public QObject
{
    Q_OBJECT
public:
    Magnetometer(QObject* parent = nullptr);

    QList<double> samples();

    void initSamples();

    inline void setBufferSize(int size) { m_bufferSize = size; }

    inline QStringList axisLabels() { return QStringList{"mGx,mGy,mGz"};};

    inline QString sensorName() { return "Magnetometer";}

    void registerDataHandler(SensorDataHandler handler);

    void deleteHandlers();

    void handleNewSampleReceived(double sample);

    void initSensor(QVariantMap configuration);
private:
    QList<double> m_samples;
    int m_bufferSize = 0;
    double m_fullScale = 0.0;
    double m_sensitivity = 0.0;
    QString m_unit = "";
    QList<SensorDataHandler> m_handlers;

};

#endif // MAGNETOMETER_H
