import QtQuick 2.0
import QtQuick.Controls 2.12

Button {
    id: button_data_row
    property string icon_path: ""
    property string label_text: ""
    height: 60
    width: 60

    Image {
        id: image_data_raw
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: icon_path
        sourceSize.height: 30
        sourceSize.width: 30
        anchors.leftMargin: 0
    }
    Text {
        anchors.horizontalCenter: image_data_raw.horizontalCenter
        anchors.top: image_data_raw.bottom
        text: label_text
    }
}
