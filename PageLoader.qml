import QtQuick 2.0

Loader {
    id: loader
    width: 480
    height: 400
    anchors.centerIn: parent
    states:[
        State {
            name: "settings"
            PropertyChanges {
                target: loader
                source:"qrc:/SettingsPage.qml"
            }
        },
        State {
            name: "dataRaw"
            PropertyChanges {
                target: loader
                source:"qrc:/DataRawPage.qml"
            }
        },
        State {
            name: "graphics"
            PropertyChanges {
                target: loader
                source:"qrc:/GraphicsPage.qml"
            }
        }
    ]

    Component.onCompleted: {
        loader.state = "dataRaw"
    }
}
