import QtQuick 2.12

Rectangle {
    color: "white"
    anchors.fill: parent
    TextInput
    {
        id: dataTextual
        text:dataProvider.displayValues
        wrapMode: Text.Wrap
        width: parent.width
    }
}
