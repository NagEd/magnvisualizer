import QtQuick 2.12
import QtQuick.Layouts 1.3
import "qrc:Chart.js" as Charts

Rectangle {
    color: "lightgrey"
    anchors.fill: parent
    Text {
        id: testo
        text: "Questa  è la pagina Graphics"
    }

    MChart {
        anchors.top: testo.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 30
        id: chartBar1

        Layout.fillWidth: true
        Layout.fillHeight: true

        chartType: Charts.ChartType.LINE

        labels: ["u","o","n","j"]/*dataProvider.plotTimeLabels*/
        values: [0,2,3,4,1]/*dataProvider.plot1Data*/
        strokeColor: "red"
        pointColor: "#ffffff"
    }
}
