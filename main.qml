import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12

ApplicationWindow {
    id: window
    width: 640
    height: 480
    visible: true
    color: "#908c8c"
    title: qsTr("Magnetometro")
    Loader {
        id: loader
        width: 480
        height: 360
        anchors.centerIn: parent
        state: "dataraw"
        states: [
            State {
                name: "dataraw"
                PropertyChanges {
                    target: loader
                    sourceComponent: id_0
                }
            },
            State {
                name: "grafici"
                PropertyChanges {
                    target: loader
                    sourceComponent: id_1
                }
            },
            State {
                name: "settings"
                PropertyChanges {
                    target: loader
                    sourceComponent: id_2
                }
            }
        ]
    }
    Component {
        id: id_0
        DataPage {}
    }
    Component {
        id: id_1
        GraphicsPage {}
    }
    Component {
        id: id_2
        SettingsPage {}
    }

    MockModel {
        id: listmodel
    }

    ListView {

        spacing: parent.height / (model.count * 2)
        width: 100
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.topMargin: spacing / 2
        anchors.bottomMargin: spacing / 2
        model: listmodel
        delegate: Clickable {
            label_text: model.name
            icon_path: model.img
            onClicked: {
                if (model.page === "exit") {
                    Qt.quit()
                }
                loader.state = model.page
            }
        }
    }
}
