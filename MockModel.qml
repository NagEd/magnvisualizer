import QtQuick 2.12
import QtQuick.Controls 2.12

ListModel {

    ListElement {
        img: "icon.png"
        name: "Data Raw"
        page: "dataraw"
    }
    ListElement {
        img: "graphics.png"
        name: "Grafici"
        page: "grafici"
    }
    ListElement {
        img: "settings.png"
        name: "Settings"
        page: "settings"
    }
    ListElement {
        img: "exit.png"
        name: "Exit"
        page: "exit"
    }
}
